# -*- coding: utf-8 -*-

#Oppg 1.

import pandas as pd
from glob import glob

files = glob('Sales_*_2019.csv')

df = pd.concat(((pd.read_csv(file, index_col=(0))) for file in files))
df.reset_index(inplace=True)
#print(df)

#Skriver til ny fil:
df.to_csv('sales2019.csv')

#Oppg 2
df = df.drop('x_t', axis=1)
df = df.drop('perf', axis=1)
print(df.head())

#Oppg 3
#Sette inn kolonne med Month
df['Month'] = df['Order Date'].str[:2]
print(df.head())



# Omgjøre Month fra streng til integer: 
df.dropna(subset=['Month'], inplace=True)
df.reset_index()
df['Month'] = pd.to_numeric(df['Month'], errors='coerce', downcast='integer') 

#print(df.dtypes())

# Legge til kollone med sum
df['Quantity Ordered'] = pd.to_numeric(df['Quantity Ordered'], errors='coerce', downcast='integer')
df['Price Each'] = pd.to_numeric(df['Price Each'], errors='coerce', downcast='integer')
df['Sum'] = df['Quantity Ordered'] * df['Price Each']

    
print(df.head())
print(df.tail())

#Gruppere pr mnd
month_grp = df.groupby(df['Month'])

#Summere salg pr mnd
sum_per_month = month_grp['Sum'].apply(lambda x: x.sum())

print(sum_per_month.head(20))


df.to_csv('sales2019_sum.csv')







